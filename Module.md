# Module orkg-client

This module contains the client for the Open Research Knowledge Graph (ORKG).

# Package org.orkg.client.rest

This package provides a client to access the ORKG REST API.
