package org.orkg.client.rest

import io.ktor.client.engine.*
import io.ktor.client.engine.cio.*

internal actual fun defaultEngine(): HttpClientEngine = CIO.create {}
