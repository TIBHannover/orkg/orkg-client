import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import org.orkg.client.rest.ORKGRestClient
import org.orkg.client.rest.resources.Predicate
import org.orkg.client.rest.resources.Predicates

suspend fun main() {
    val orkg = ORKGRestClient()
    // Custom API
    println(orkg.predicates.byId("P500"))
    // Direct access
    println(orkg.client.get(Predicates.ById(id = "SAME_AS")).body<Predicate>())
}
