package org.orkg.client.rest

import io.ktor.client.engine.*
import io.ktor.client.engine.js.*

internal actual fun defaultEngine(): HttpClientEngine = Js.create()
