package org.orkg.client.rest.resources

interface ProvenanceMetadata {
    val createdBy: String // FIXME
    val createdAt: String // FIXME
}
