package org.orkg.client.rest.resources

import io.ktor.resources.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Type-safe resources definition for the Ktor "Resources" plugin.
 */
@Serializable
@Resource("/predicates")
@Suppress("unused")
class Predicates {
    @Serializable
    @Resource("/{id}")
    class ById(val parent: Predicates = Predicates(), val id: String)
}


/**
 * A data transfer object (DTO) as returned by the API.
 */
@Serializable
data class Predicate(
    val id: String,
    val label: String,
    @SerialName("created_by") override val createdBy: String,
    @SerialName("created_at") override val createdAt: String,
) : ProvenanceMetadata
