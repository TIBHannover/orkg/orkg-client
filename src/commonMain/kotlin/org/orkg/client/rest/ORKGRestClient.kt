package org.orkg.client.rest

import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.plugins.resources.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.utils.io.core.*
import kotlinx.serialization.json.Json
import org.orkg.client.rest.actions.PredicateActions


const val ORKG_BASE_URL = "https://incubating.orkg.org"
const val ORKG_API_URL = "$ORKG_BASE_URL/api"

internal expect fun defaultEngine(): HttpClientEngine

/**
 * Create a new client for the ORKG REST API.
 *
 * @param baseUrl The base URL for the API to connect to.
 * @param engine The [HttpClientEngine] to use. Defaults to the engine that is default for the target.
 * @param loglevel The log level to use. Defaults to [LogLevel.NONE], meaning logging is disabled.
 */
class ORKGRestClient(baseUrl: String, engine: HttpClientEngine = defaultEngine(), loglevel: LogLevel = LogLevel.NONE) :
    Closeable {
    constructor(engine: HttpClientEngine = defaultEngine(), loglevel: LogLevel = LogLevel.NONE) : this(
        ORKG_API_URL, engine, loglevel
    )

    private val block: HttpClientConfig<*>.() -> Unit = {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
        install(Resources)
        install(Logging) {
            level = loglevel
        }

        defaultRequest {
            url(if (baseUrl.endsWith("/")) baseUrl else "$baseUrl/")
        }
    }
    val client: HttpClient = HttpClient(engine, block)

    override fun close() {
        client.close()
    }

    val predicates: PredicateActions = PredicateActions(client)
}
