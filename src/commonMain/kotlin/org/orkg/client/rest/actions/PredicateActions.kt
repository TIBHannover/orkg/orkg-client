package org.orkg.client.rest.actions

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.http.*
import org.orkg.client.rest.resources.Predicate
import org.orkg.client.rest.resources.Predicates

/* Provides convenient access to the API. */
class PredicateActions(private val client: HttpClient) {
    suspend fun byId(id: String): Predicate? {
        val response = client.get(Predicates.ById(id = id))
        return if (response.status == HttpStatusCode.OK) response.body() else null
    }
}
