package org.orkg.client.rest

import io.ktor.client.engine.mock.*
import io.ktor.http.*
import io.ktor.utils.io.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.orkg.client.rest.resources.Predicate
import kotlin.test.Test
import kotlin.test.assertEquals


@OptIn(ExperimentalCoroutinesApi::class)
class OrkgClientTest {
    @Test
    fun fetchesSinglePredicate() = runTest {
        val mockEngine = MockEngine {
            respond(
                content = ByteReadChannel("""{"id":"P1234","label":"some label","created_by":"7ea4620a-1eab-4134-ae88-2f639b6fadd8","created_at":"2023-06-04T15:46:40.614655505+02:00"}"""),
                status = HttpStatusCode.OK,
                headers = headersOf(HttpHeaders.ContentType, "application/json")
            )
        }
        val client = ORKGRestClient(mockEngine)

        assertEquals(
            Predicate(
                id = "P1234",
                label = "some label",
                createdBy = "7ea4620a-1eab-4134-ae88-2f639b6fadd8",
                createdAt = "2023-06-04T15:46:40.614655505+02:00"
            ), client.predicates.byId("P1234")
        )
    }
}
